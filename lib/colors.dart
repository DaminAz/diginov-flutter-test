import 'package:flutter/cupertino.dart';

class CustomColors {
  // static const Color GreyBackground = Color.fromRGBO(249, 252, 255, 1);

  static const Color teal = Color(0xff4fd0b1);
  static const Color lightTeal = Color(0xffe6fff9);
  static const Color grey = Color(0xfff7f8fa);
  static const Color darkGrey = Color(0xff8e939e);
  static const Color yellow = Color(0xffffdd5f);
}
