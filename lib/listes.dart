class Reservation {
  int nbHours;
  String hours;
  int price;
  String priceText;
  Reservation({
    required this.nbHours,
    required this.price,
    required this.hours,
    required this.priceText,
  });
}

List reservationlist = [
  Reservation(
    nbHours: 1,
    price: 1,
    hours: '1',
    priceText: '1',
  ),
  Reservation(
    nbHours: 2,
    price: 2,
    hours: '2',
    priceText: '2',
  ),
  Reservation(
    nbHours: 4,
    price: 3,
    hours: '4',
    priceText: '3',
  ),
  Reservation(
    nbHours: 8,
    price: 5,
    hours: '8',
    priceText: '5',
  ),
  Reservation(
    nbHours: 10,
    price: 8,
    hours: '10',
    priceText: '8',
  ),
  Reservation(
    nbHours: 14,
    price: 11,
    hours: '14',
    priceText: '11',
  ),
  Reservation(
    nbHours: 24,
    price: 20,
    hours: '24',
    priceText: '20',
  ),
];
