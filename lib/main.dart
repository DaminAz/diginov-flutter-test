import 'package:diginov_test/colors.dart';
import 'package:diginov_test/details.dart';
import 'package:diginov_test/listes.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      debugShowCheckedModeBanner: false,
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      home: const MyHomePage(),
    );
  }
}

class MyHomePage extends StatefulWidget {
  const MyHomePage({Key? key}) : super(key: key);

  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  @override
  Widget build(BuildContext context) {
    double width = MediaQuery.of(context).size.width;
    double height = MediaQuery.of(context).size.height;
    final ValueNotifier<int> selectedIndex = ValueNotifier<int>(0);
    final ValueNotifier<bool> hasChanged = ValueNotifier<bool>(false);

    void _setSelectedIndex(int index) {
      selectedIndex.value = index;
      hasChanged.value = true;
    }

    Future<void> openDialog() async {
      return showDialog<void>(
        context: context,
        barrierDismissible: false, // user must tap button! if it's false
        builder: (BuildContext context) {
          return AlertDialog(
            content: Column(
              mainAxisSize: MainAxisSize.min,
              children: [
                Padding(
                  padding: const EdgeInsets.only(top: 0, bottom: 10),
                  child: SizedBox(
                    height: 30,
                    width: width * .57,
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: const [
                        Text(
                          "Durée",
                          style: TextStyle(
                              fontSize: 16,
                              fontWeight: FontWeight.w600,
                              color: CustomColors.teal),
                        ),
                        Text(
                          "Montant",
                          style: TextStyle(
                              fontSize: 16,
                              fontWeight: FontWeight.w600,
                              color: CustomColors.teal),
                        ),
                      ],
                    ),
                  ),
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    SizedBox(
                      height: 200,
                      width: width * .55,
                      child: ListView.separated(
                        separatorBuilder: (context, index) {
                          return const Padding(
                              padding: EdgeInsets.only(top: 10, bottom: 10));
                        },
                        padding: const EdgeInsets.only(top: 5, bottom: 5),
                        shrinkWrap: true,
                        itemCount: reservationlist.length,
                        itemBuilder: (BuildContext context, int index) {
                          return GestureDetector(
                            onTap: () {
                              _setSelectedIndex(index);
                              // print(
                              //     "selectedIndex = ${selectedIndex.value} and index = $index");
                            },
                            child: ValueListenableBuilder<int>(
                                valueListenable: selectedIndex,
                                builder: (BuildContext context,
                                    int selectedIndexValue, Widget? child) {
                                  return Row(
                                    mainAxisAlignment:
                                        MainAxisAlignment.spaceBetween,
                                    children: [
                                      Center(
                                        child: Text(
                                          "${reservationlist[index].hours} Heures",
                                          style: TextStyle(
                                              fontSize: 20,
                                              fontWeight: FontWeight.w600,
                                              color:
                                                  selectedIndex.value == index
                                                      ? Colors.black
                                                      : CustomColors.darkGrey),
                                        ),
                                      ),
                                      Center(
                                        child: Text(
                                          "${reservationlist[index].priceText} Euros",
                                          style: TextStyle(
                                              fontSize: 20,
                                              fontWeight: FontWeight.w600,
                                              color:
                                                  selectedIndex.value == index
                                                      ? Colors.black
                                                      : CustomColors.darkGrey),
                                        ),
                                      )
                                    ],
                                  );
                                }),
                          );
                        },
                      ),
                    ),
                  ],
                ),
                Padding(
                  padding: const EdgeInsets.only(top: 20, bottom: 0),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      TextButton(
                        child: const Text("Annuler",
                            style: TextStyle(fontSize: 14)),
                        style: ButtonStyle(
                            padding: MaterialStateProperty.all<EdgeInsets>(
                                const EdgeInsets.all(10)),
                            foregroundColor: MaterialStateProperty.all<Color>(
                              CustomColors.teal,
                            ),
                            shape: MaterialStateProperty
                                .all<RoundedRectangleBorder>(
                                    RoundedRectangleBorder(
                                        borderRadius:
                                            BorderRadius.circular(10.0),
                                        side: const BorderSide(
                                          color: CustomColors.teal,
                                        )))),
                        onPressed: () {
                          setState(() {
                            Navigator.pop(context);
                          });
                        },
                      ),
                      TextButton(
                          child: const Text("Confirmer",
                              style: TextStyle(fontSize: 14)),
                          style: ButtonStyle(
                              padding: MaterialStateProperty.all<EdgeInsets>(
                                  const EdgeInsets.all(10)),
                              foregroundColor: MaterialStateProperty.all<Color>(
                                  Colors.white),
                              backgroundColor: MaterialStateProperty.all<Color>(
                                CustomColors.teal,
                              ),
                              shape: MaterialStateProperty.all<
                                      RoundedRectangleBorder>(
                                  RoundedRectangleBorder(
                                      borderRadius: BorderRadius.circular(10.0),
                                      side: const BorderSide(
                                        color: CustomColors.teal,
                                      )))),
                          onPressed: () => Navigator.pop(context)),
                    ],
                  ),
                )
              ],
            ),
          );
        },
      );
    }

    return Scaffold(
      backgroundColor: Colors.white,
      appBar: AppBar(
        backgroundColor: Colors.transparent,
        elevation: 0.0,
        toolbarHeight: 70,
        title: const Text("Diginov Test"),
        centerTitle: true,
        flexibleSpace: Container(
          decoration: const BoxDecoration(
              borderRadius: BorderRadius.only(
                  bottomLeft: Radius.circular(20),
                  bottomRight: Radius.circular(0)),
              color: CustomColors.yellow),
        ),
        systemOverlayStyle: SystemUiOverlayStyle.light,
      ),
      body: Column(
        children: [
          Align(
            alignment: Alignment.topRight,
            child: Stack(children: [
              Container(
                width: 30,
                height: 30,
                decoration: const BoxDecoration(
                    borderRadius:
                        BorderRadius.only(bottomLeft: Radius.circular(30)),
                    color: CustomColors.yellow),
              ),
              Container(
                width: 30,
                height: 30,
                decoration: const BoxDecoration(
                    borderRadius:
                        BorderRadius.only(topRight: Radius.circular(30)),
                    color: Colors.white),
              ),
            ]),
          ),
          Expanded(
            child: SingleChildScrollView(
              child: Column(
                children: [
                  const Padding(
                    padding: EdgeInsets.only(top: 10, bottom: 50),
                    child: Center(
                      child: Text(
                        "Veuillez choisir la durée de réservation",
                        style: TextStyle(
                          fontSize: 16,
                          fontWeight: FontWeight.w800,
                        ),
                      ),
                    ),
                  ),
                  SizedBox(
                    child: Row(
                        mainAxisAlignment: MainAxisAlignment.start,
                        children: [
                          Container(
                            decoration: const BoxDecoration(
                              borderRadius: BorderRadius.only(
                                  topRight: Radius.circular(20),
                                  bottomRight: Radius.circular(20)),
                              color: CustomColors.grey,
                            ),
                            child: Column(
                              children: [
                                const Padding(
                                  padding: EdgeInsets.all(15),
                                  child: Text(
                                    "Liste des prix",
                                    style: TextStyle(
                                      fontSize: 16,
                                      fontWeight: FontWeight.w600,
                                      color: CustomColors.teal,
                                    ),
                                  ),
                                ),
                                SizedBox(
                                  height: 220,
                                  width: width * .45,
                                  child: ListView.separated(
                                    padding: const EdgeInsets.only(
                                        top: 5, bottom: 5),
                                    separatorBuilder: (context, index) {
                                      return Padding(
                                        padding:
                                            const EdgeInsets.only(left: 22.5),
                                        child: Align(
                                          alignment: Alignment.centerLeft,
                                          child: Column(
                                            children: [
                                              Container(
                                                decoration: const BoxDecoration(
                                                  borderRadius:
                                                      BorderRadius.all(
                                                          Radius.circular(50)),
                                                  color: CustomColors.teal,
                                                ),
                                                width: 2.2,
                                                height: 2.2,
                                              ),
                                              const Padding(
                                                  padding: EdgeInsets.only(
                                                      top: 2, bottom: 2)),
                                              Container(
                                                decoration: const BoxDecoration(
                                                  borderRadius:
                                                      BorderRadius.all(
                                                          Radius.circular(50)),
                                                  color: CustomColors.teal,
                                                ),
                                                width: 2.2,
                                                height: 2.2,
                                              ),
                                              const Padding(
                                                  padding: EdgeInsets.only(
                                                      top: 2, bottom: 2)),
                                              Container(
                                                decoration: const BoxDecoration(
                                                  borderRadius:
                                                      BorderRadius.all(
                                                          Radius.circular(50)),
                                                  color: CustomColors.teal,
                                                ),
                                                width: 2.2,
                                                height: 2.2,
                                              ),
                                              const Padding(
                                                  padding: EdgeInsets.only(
                                                      top: 2, bottom: 2)),
                                              Container(
                                                decoration: const BoxDecoration(
                                                  borderRadius:
                                                      BorderRadius.all(
                                                          Radius.circular(50)),
                                                  color: CustomColors.teal,
                                                ),
                                                width: 2.2,
                                                height: 2.2,
                                              ),
                                              const Padding(
                                                  padding: EdgeInsets.only(
                                                      top: 2, bottom: 2)),
                                              Container(
                                                decoration: const BoxDecoration(
                                                  borderRadius:
                                                      BorderRadius.all(
                                                          Radius.circular(50)),
                                                  color: CustomColors.teal,
                                                ),
                                                width: 2.2,
                                                height: 2.2,
                                              ),
                                              const Padding(
                                                  padding: EdgeInsets.only(
                                                      top: 2, bottom: 2)),
                                            ],
                                          ),
                                        ),
                                      );
                                    },
                                    shrinkWrap: true,
                                    itemCount: reservationlist.length,
                                    itemBuilder:
                                        (BuildContext context, int index) {
                                      return Row(
                                        // mainAxisAlignment: MainAxisAlignment.spaceAround,
                                        children: [
                                          Padding(
                                            padding:
                                                const EdgeInsets.only(left: 20),
                                            child: Align(
                                              alignment: Alignment.centerLeft,
                                              child: Container(
                                                color: Colors.yellow,
                                                width: 7,
                                                height: 7,
                                              ),
                                            ),
                                          ),
                                          Padding(
                                            padding:
                                                const EdgeInsets.only(left: 15),
                                            child: Text(
                                              "${reservationlist[index].nbHours.toString()}H",
                                              style: const TextStyle(
                                                fontSize: 14,
                                                fontWeight: FontWeight.w600,
                                              ),
                                            ),
                                          ),
                                          const Expanded(
                                            child: Align(
                                              alignment: Alignment.center,
                                              child: Text(
                                                ".......",
                                                style: TextStyle(
                                                  fontSize: 14,
                                                  fontWeight: FontWeight.w600,
                                                  color: CustomColors.teal,
                                                ),
                                              ),
                                            ),
                                          ),
                                          Padding(
                                            padding: const EdgeInsets.only(
                                                right: 10),
                                            child: Align(
                                              alignment: Alignment.centerRight,
                                              child: Text(
                                                "${reservationlist[index].price.toString()}€",
                                                style: const TextStyle(
                                                  fontSize: 14,
                                                  fontWeight: FontWeight.w600,
                                                ),
                                              ),
                                            ),
                                          ),
                                        ],
                                      );
                                    },
                                  ),
                                ),
                              ],
                            ),
                          ),
                          const Expanded(
                            child: Center(),
                          ),
                          Container(
                              padding: const EdgeInsets.only(right: 30),
                              height: 260,
                              width: width * .48,
                              child: Stack(
                                children: [
                                  Container(
                                    height: height,
                                    decoration: BoxDecoration(
                                        borderRadius: const BorderRadius.all(
                                            Radius.circular(20)),
                                        color: Colors.white,
                                        border: Border.all(
                                            color: CustomColors.teal,
                                            width: 5)),
                                    child: const Center(
                                      child: Text(
                                        "12",
                                        style: TextStyle(
                                          fontSize: 60,
                                          fontWeight: FontWeight.bold,
                                          color: CustomColors.teal,
                                        ),
                                      ),
                                    ),
                                  ),
                                  Align(
                                    alignment: Alignment.topLeft,
                                    child: Container(
                                      color: Colors.white,
                                      child: const Text(
                                        "Small Box",
                                        style: TextStyle(
                                          fontSize: 30,
                                          fontWeight: FontWeight.bold,
                                          color: CustomColors.teal,
                                        ),
                                      ),
                                      width: 90,
                                      height: 90,
                                    ),
                                  ),
                                ],
                              )),
                        ]),
                  ),
                  Padding(
                    padding: const EdgeInsets.only(
                        right: 20, left: 20, top: 50, bottom: 10),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: const [
                        Text(
                          "Durée",
                          style: TextStyle(
                              fontSize: 16,
                              fontWeight: FontWeight.w600,
                              color: CustomColors.teal),
                        ),
                        Text(
                          "Montant",
                          style: TextStyle(
                              fontSize: 16,
                              fontWeight: FontWeight.w600,
                              color: CustomColors.teal),
                        ),
                      ],
                    ),
                  ),
                  Container(
                      decoration: BoxDecoration(
                          borderRadius:
                              const BorderRadius.all(Radius.circular(0)),
                          color: Colors.white,
                          border: Border.all(
                              color: CustomColors.yellow, width: 1.5)),
                      child: GestureDetector(
                        onTap: () {
                          openDialog();
                        },
                        child: ValueListenableBuilder<int>(
                          valueListenable: selectedIndex,
                          builder: (BuildContext context,
                              int selectedIndexValue, Widget? child) {
                            return ValueListenableBuilder<bool>(
                              valueListenable: hasChanged,
                              builder: (BuildContext context, bool hasChanged,
                                  Widget? child) {
                                return Row(
                                  mainAxisAlignment:
                                      MainAxisAlignment.spaceAround,
                                  children: [
                                    Text(
                                      "${reservationlist[selectedIndexValue].hours} Heures",
                                      style: TextStyle(
                                        fontSize: 20,
                                        fontWeight: FontWeight.w600,
                                        color: hasChanged
                                            ? Colors.black
                                            : CustomColors.darkGrey,
                                      ),
                                    ),
                                    Column(
                                      children: const [
                                        Icon(
                                          (Icons.keyboard_arrow_up),
                                          color: CustomColors.teal,
                                          size: 30,
                                        ),
                                        Icon(
                                          (Icons.keyboard_arrow_down),
                                          color: CustomColors.teal,
                                          size: 30,
                                        ),
                                      ],
                                    ),
                                    Text(
                                      "${reservationlist[selectedIndexValue].priceText} Euros",
                                      style: TextStyle(
                                        fontSize: 20,
                                        fontWeight: FontWeight.w600,
                                        color: hasChanged
                                            ? Colors.black
                                            : CustomColors.darkGrey,
                                      ),
                                    ),
                                  ],
                                );
                              },
                            );
                          },
                        ),
                      )),
                  ValueListenableBuilder<bool>(
                    valueListenable: hasChanged,
                    builder:
                        (BuildContext context, bool hasChanged, Widget? child) {
                      return SizedBox(
                        width: 350,
                        child: Padding(
                          padding: const EdgeInsets.only(top: 30, bottom: 20),
                          child: TextButton(
                            child: const Text("Confirmer",
                                style: TextStyle(fontSize: 16)),
                            style: ButtonStyle(
                                padding: MaterialStateProperty.all<EdgeInsets>(
                                    const EdgeInsets.all(20)),
                                foregroundColor:
                                    MaterialStateProperty.all<Color>(
                                  hasChanged
                                      ? Colors.white
                                      : CustomColors.darkGrey,
                                ),
                                backgroundColor:
                                    MaterialStateProperty.all<Color>(
                                  hasChanged ? CustomColors.teal : Colors.white,
                                ),
                                shape: MaterialStateProperty.all<
                                        RoundedRectangleBorder>(
                                    RoundedRectangleBorder(
                                        borderRadius:
                                            BorderRadius.circular(10.0),
                                        side: BorderSide(
                                          color: hasChanged
                                              ? Colors.white
                                              : CustomColors.darkGrey,
                                        )))),
                            onPressed: () {
                              Navigator.of(context).push(
                                MaterialPageRoute(
                                  builder: (context) => Details(
                                    selectedIndex: selectedIndex.value,
                                  ),
                                ),
                              );
                            },
                          ),
                        ),
                      );
                    },
                  ),
                ],
              ),
            ),
          ),
        ],
      ),
    );
  }
}
