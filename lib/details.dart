import 'package:diginov_test/colors.dart';
import 'package:diginov_test/listes.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

class Details extends StatefulWidget {
  final int selectedIndex;
  const Details({Key? key, required this.selectedIndex}) : super(key: key);

  @override
  State<Details> createState() => _DetailsState();
}

class _DetailsState extends State<Details> {
  @override
  Widget build(BuildContext context) {
    double width = MediaQuery.of(context).size.width;
    double height = MediaQuery.of(context).size.height;
    final ValueNotifier<bool> isLoading = ValueNotifier<bool>(false);
    final ValueNotifier<bool> isLoaded = ValueNotifier<bool>(false);
    void _setIsLoadingTrue() {
      isLoading.value = true;
    }

    return Scaffold(
      backgroundColor: Colors.white,
      appBar: AppBar(
        backgroundColor: Colors.transparent,
        elevation: 0.0,
        toolbarHeight: 70,
        title: const Text("Diginov Test"),
        centerTitle: true,
        flexibleSpace: Container(
          decoration: const BoxDecoration(
              borderRadius: BorderRadius.only(
                  bottomLeft: Radius.circular(20),
                  bottomRight: Radius.circular(0)),
              color: CustomColors.yellow),
        ),
        systemOverlayStyle: SystemUiOverlayStyle.light,
      ),
      body: Container(
        color: CustomColors.teal,
        child: Column(
          mainAxisSize: MainAxisSize.min,
          children: [
            Container(
              color: Colors.white,
              child: Align(
                alignment: Alignment.topRight,
                child: Stack(children: [
                  Container(
                    width: 30,
                    height: 30,
                    decoration: const BoxDecoration(
                        borderRadius:
                            BorderRadius.only(bottomLeft: Radius.circular(30)),
                        color: CustomColors.yellow),
                  ),
                  Container(
                    width: 30,
                    height: 30,
                    decoration: const BoxDecoration(
                        borderRadius:
                            BorderRadius.only(topRight: Radius.circular(30)),
                        color: Colors.white),
                  ),
                ]),
              ),
            ),
            Flexible(
              child: SingleChildScrollView(
                child: Container(
                  height: height,
                  child: Column(
                    mainAxisSize: MainAxisSize.min,
                    children: [
                      Container(
                        decoration: const BoxDecoration(
                          borderRadius: BorderRadius.only(
                              bottomLeft: Radius.circular(30)),
                          color: Colors.white,
                        ),
                        child: Column(
                          mainAxisSize: MainAxisSize.min,
                          children: [
                            const Padding(
                              padding: EdgeInsets.only(
                                  left: 15, top: 10, bottom: 10),
                              child: Align(
                                alignment: Alignment.centerLeft,
                                child: Text(
                                  "Details de la réservation.",
                                  style: TextStyle(
                                    fontSize: 16,
                                    fontWeight: FontWeight.w800,
                                  ),
                                ),
                              ),
                            ),
                            Padding(
                              padding: const EdgeInsets.only(
                                  top: 20, bottom: 40, left: 20, right: 20),
                              child: Stack(
                                children: [
                                  Container(
                                    width: width * .85,
                                    height: height * .20,
                                    padding: const EdgeInsets.all(10),
                                    decoration: const BoxDecoration(
                                      borderRadius:
                                          BorderRadius.all(Radius.circular(20)),
                                      color: CustomColors.lightTeal,
                                    ),
                                    child: Align(
                                      alignment: Alignment.centerLeft,
                                      child: Column(
                                        mainAxisAlignment:
                                            MainAxisAlignment.center,
                                        crossAxisAlignment:
                                            CrossAxisAlignment.start,
                                        children: [
                                          Container(
                                            padding: const EdgeInsets.only(
                                                top: 5,
                                                bottom: 5,
                                                left: 10,
                                                right: 10),
                                            child: const Text("● Cles"),
                                          ),
                                          Container(
                                            padding: const EdgeInsets.only(
                                                top: 5,
                                                bottom: 5,
                                                left: 10,
                                                right: 10),
                                            child: const Text("● Bouteilles"),
                                          ),
                                          Container(
                                            padding: const EdgeInsets.only(
                                                top: 5,
                                                bottom: 5,
                                                left: 10,
                                                right: 10),
                                            child:
                                                const Text("● Porte feuille"),
                                          ),
                                          Container(
                                            padding: const EdgeInsets.only(
                                                top: 5,
                                                bottom: 5,
                                                left: 10,
                                                right: 10),
                                            child: const Text("..."),
                                          ),
                                        ],
                                      ),
                                    ),
                                  ),
                                  Align(
                                    alignment: Alignment.centerRight,
                                    child: Container(
                                      child: const Center(
                                        child: Text(
                                          "Small Box",
                                          style: TextStyle(
                                            fontSize: 40,
                                            fontWeight: FontWeight.bold,
                                            color: CustomColors.teal,
                                          ),
                                          textAlign: TextAlign.center,
                                        ),
                                      ),
                                      width: width * .425,
                                      height: height * .20,
                                      padding: const EdgeInsets.all(10),
                                      decoration: BoxDecoration(
                                        borderRadius: const BorderRadius.all(
                                            Radius.circular(20)),
                                        color: Colors.white,
                                        border: Border.all(
                                            color: CustomColors.teal, width: 4),
                                      ),
                                    ),
                                  ),
                                ],
                              ),
                            )
                          ],
                        ),
                      ),
                      Padding(
                        padding: const EdgeInsets.only(
                            top: 30, bottom: 10, left: 30, right: 30),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            const Text(
                              "Numéro du casier",
                              style: TextStyle(
                                color: Colors.white,
                                fontSize: 20,
                                fontWeight: FontWeight.w600,
                              ),
                            ),
                            Container(
                              child: const Center(
                                child: Text(
                                  "12",
                                  style: TextStyle(
                                    fontSize: 20,
                                    fontWeight: FontWeight.bold,
                                    color: CustomColors.teal,
                                  ),
                                  textAlign: TextAlign.center,
                                ),
                              ),
                              width: 50,
                              height: 50,
                              padding: const EdgeInsets.all(10),
                              decoration: BoxDecoration(
                                borderRadius:
                                    const BorderRadius.all(Radius.circular(10)),
                                color: Colors.white,
                                border: Border.all(
                                  color: CustomColors.yellow,
                                  width: 2,
                                ),
                              ),
                            ),
                          ],
                        ),
                      ),
                      Padding(
                        padding: const EdgeInsets.only(
                            left: 30, right: 30, top: 10, bottom: 0),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: const [
                            Text(
                              "Durée",
                              style: TextStyle(
                                  fontSize: 16,
                                  fontWeight: FontWeight.w400,
                                  color: CustomColors.yellow),
                            ),
                            Text(
                              "Montant",
                              style: TextStyle(
                                  fontSize: 16,
                                  fontWeight: FontWeight.w400,
                                  color: CustomColors.yellow),
                            ),
                          ],
                        ),
                      ),
                      Center(
                        child: Padding(
                          padding: const EdgeInsets.only(
                              left: 30, right: 30, top: 10, bottom: 30),
                          child: Container(
                            decoration: const BoxDecoration(
                              border: Border(
                                top: BorderSide(
                                    color: CustomColors.yellow, width: 1),
                                bottom: BorderSide(
                                    color: CustomColors.yellow, width: 1),
                              ),
                            ),
                            child: Padding(
                              padding:
                                  const EdgeInsets.only(top: 10, bottom: 10),
                              child: Row(
                                mainAxisAlignment:
                                    MainAxisAlignment.spaceBetween,
                                children: [
                                  Text(
                                    "${reservationlist[widget.selectedIndex].hours} Heures",
                                    style: const TextStyle(
                                        fontSize: 18,
                                        fontWeight: FontWeight.w400,
                                        color: Colors.white),
                                  ),
                                  Text(
                                    "${reservationlist[widget.selectedIndex].priceText} Euros",
                                    style: const TextStyle(
                                        fontSize: 18,
                                        fontWeight: FontWeight.w400,
                                        color: Colors.white),
                                  ),
                                ],
                              ),
                            ),
                          ),
                        ),
                      ),
                      Expanded(
                        child: Container(
                          // height: 150,
                          width: width,
                          padding: const EdgeInsets.all(10),
                          decoration: const BoxDecoration(
                            borderRadius: BorderRadius.only(
                                topLeft: Radius.circular(25),
                                topRight: Radius.circular(25)),
                            color: Colors.white,
                          ),
                          child: Column(
                            children: [
                              Padding(
                                padding: const EdgeInsets.all(15),
                                child: Row(
                                  mainAxisAlignment:
                                      MainAxisAlignment.spaceBetween,
                                  children: [
                                    const Text(
                                      "TOTAL",
                                      style: TextStyle(
                                          fontSize: 20,
                                          fontWeight: FontWeight.w600,
                                          color: Colors.black),
                                    ),
                                    Text(
                                      "${reservationlist[widget.selectedIndex].price}€",
                                      style: const TextStyle(
                                          fontSize: 20,
                                          fontWeight: FontWeight.w600,
                                          color: Colors.black),
                                    ),
                                  ],
                                ),
                              ),
                              Flexible(
                                child: SizedBox(
                                  width: width * .9,
                                  child: TextButton(
                                      child: const Text("Payer",
                                          style: TextStyle(fontSize: 18)),
                                      style: ButtonStyle(
                                        padding: MaterialStateProperty.all<
                                                EdgeInsets>(
                                            const EdgeInsets.all(20)),
                                        foregroundColor:
                                            MaterialStateProperty.all<Color>(
                                                Colors.white),
                                        backgroundColor:
                                            MaterialStateProperty.all<Color>(
                                          CustomColors.teal,
                                        ),
                                        shape: MaterialStateProperty.all<
                                            RoundedRectangleBorder>(
                                          RoundedRectangleBorder(
                                            borderRadius:
                                                BorderRadius.circular(10.0),
                                            side: const BorderSide(
                                              color: CustomColors.teal,
                                            ),
                                          ),
                                        ),
                                      ),
                                      onPressed: () {
                                        showModalBottomSheet<void>(
                                          shape: const RoundedRectangleBorder(
                                            borderRadius: BorderRadius.only(
                                              topLeft: Radius.circular(10),
                                              topRight: Radius.circular(10),
                                            ),
                                          ),
                                          context: context,
                                          builder: (BuildContext context) {
                                            return Padding(
                                              padding: const EdgeInsets.all(20),
                                              child:
                                                  ValueListenableBuilder<bool>(
                                                valueListenable: isLoaded,
                                                builder: (BuildContext context,
                                                    bool isLoadedValue,
                                                    Widget? child) {
                                                  return Column(
                                                    children: [
                                                      Container(
                                                        height: 8,
                                                        width: width / 3,
                                                        decoration:
                                                            const BoxDecoration(
                                                          borderRadius:
                                                              BorderRadius.all(
                                                            Radius.circular(25),
                                                          ),
                                                          color: CustomColors
                                                              .yellow,
                                                        ),
                                                      ),
                                                      isLoaded.value
                                                          ? const Center()
                                                          : Padding(
                                                              padding:
                                                                  const EdgeInsets
                                                                          .all(
                                                                      25.0),
                                                              child: Text(
                                                                "Payer ${reservationlist[widget.selectedIndex].priceText} Euros",
                                                                style: const TextStyle(
                                                                    fontSize:
                                                                        20,
                                                                    fontWeight:
                                                                        FontWeight
                                                                            .w500,
                                                                    color: Colors
                                                                        .black87),
                                                              ),
                                                            ),
                                                      const Expanded(
                                                          child: Center()),
                                                      ValueListenableBuilder<
                                                          bool>(
                                                        valueListenable:
                                                            isLoading,
                                                        builder: (BuildContext
                                                                context,
                                                            bool isLoadingValue,
                                                            Widget? child) {
                                                          return isLoaded.value
                                                              ? Column(
                                                                  mainAxisAlignment:
                                                                      MainAxisAlignment
                                                                          .spaceAround,
                                                                  children: [
                                                                    SizedBox(
                                                                      height:
                                                                          80,
                                                                      child: Image
                                                                          .asset(
                                                                              'assets/images/credit.png'),
                                                                    ),
                                                                    const Padding(
                                                                      padding:
                                                                          EdgeInsets.all(
                                                                              25.0),
                                                                      child:
                                                                          Center(
                                                                        child:
                                                                            Text(
                                                                          "votre paiement a été effectué avec succès!",
                                                                          style: TextStyle(
                                                                              fontSize: 20,
                                                                              fontWeight: FontWeight.w500,
                                                                              color: Colors.black87),
                                                                          textAlign:
                                                                              TextAlign.center,
                                                                        ),
                                                                      ),
                                                                    ),
                                                                    Container(
                                                                      padding: const EdgeInsets
                                                                              .only(
                                                                          bottom:
                                                                              20),
                                                                      width:
                                                                          width *
                                                                              .9,
                                                                      child:
                                                                          TextButton(
                                                                        child: const Text(
                                                                            "SUIVANT",
                                                                            style:
                                                                                TextStyle(fontSize: 18)),
                                                                        style:
                                                                            ButtonStyle(
                                                                          padding:
                                                                              MaterialStateProperty.all<EdgeInsets>(const EdgeInsets.all(20)),
                                                                          foregroundColor:
                                                                              MaterialStateProperty.all<Color>(Colors.white),
                                                                          backgroundColor:
                                                                              MaterialStateProperty.all<Color>(
                                                                            CustomColors.teal,
                                                                          ),
                                                                          shape:
                                                                              MaterialStateProperty.all<RoundedRectangleBorder>(
                                                                            RoundedRectangleBorder(
                                                                              borderRadius: BorderRadius.circular(10.0),
                                                                              side: const BorderSide(
                                                                                color: CustomColors.teal,
                                                                              ),
                                                                            ),
                                                                          ),
                                                                        ),
                                                                        onPressed:
                                                                            () {
                                                                          setState(
                                                                              () {
                                                                            Navigator.pop(context);
                                                                          });
                                                                        },
                                                                      ),
                                                                    ),
                                                                  ],
                                                                )
                                                              : isLoading.value
                                                                  ? const Padding(
                                                                      padding: EdgeInsets.only(
                                                                          bottom:
                                                                              20),
                                                                      child:
                                                                          CircularProgressIndicator(
                                                                        backgroundColor:
                                                                            Colors.transparent,
                                                                        valueColor:
                                                                            AlwaysStoppedAnimation<Color>(CustomColors.teal),
                                                                      ),
                                                                    )
                                                                  : Container(
                                                                      padding: const EdgeInsets
                                                                              .only(
                                                                          bottom:
                                                                              20),
                                                                      width:
                                                                          width *
                                                                              .9,
                                                                      child:
                                                                          TextButton(
                                                                        child: Text(
                                                                            "Payer ${reservationlist[widget.selectedIndex].priceText}€",
                                                                            style:
                                                                                const TextStyle(fontSize: 18)),
                                                                        style:
                                                                            ButtonStyle(
                                                                          padding:
                                                                              MaterialStateProperty.all<EdgeInsets>(const EdgeInsets.all(20)),
                                                                          foregroundColor:
                                                                              MaterialStateProperty.all<Color>(Colors.white),
                                                                          backgroundColor:
                                                                              MaterialStateProperty.all<Color>(
                                                                            CustomColors.teal,
                                                                          ),
                                                                          shape:
                                                                              MaterialStateProperty.all<RoundedRectangleBorder>(
                                                                            RoundedRectangleBorder(
                                                                              borderRadius: BorderRadius.circular(10.0),
                                                                              side: const BorderSide(
                                                                                color: CustomColors.teal,
                                                                              ),
                                                                            ),
                                                                          ),
                                                                        ),
                                                                        onPressed:
                                                                            () {
                                                                          print(
                                                                              "${isLoading.value}");
                                                                          _setIsLoadingTrue();
                                                                          print(
                                                                              "${isLoading.value}");
                                                                          Future.delayed(
                                                                              const Duration(seconds: 2),
                                                                              (() {
                                                                            isLoading.value =
                                                                                false;
                                                                            isLoaded.value =
                                                                                true;
                                                                          }));
                                                                        },
                                                                      ),
                                                                    );
                                                        },
                                                      ),
                                                      const Divider(
                                                        thickness: 1,
                                                        color:
                                                            CustomColors.yellow,
                                                      ),
                                                    ],
                                                  );
                                                },
                                              ),
                                            );
                                          },
                                        );
                                      }),
                                ),
                              ),
                            ],
                          ),
                        ),
                      ),
                    ],
                  ),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
